title Management Dashboard Web App

participant Web App Form
participant Wallet
participant DMS

Web App Form <- DMS: Get Claim
Web App Form -> Wallet: Unlock Assets In Contract

alt success
Web App Form -> DMS: <<send status>> success
else failure
Web App Form -> DMS: <<send status>> failure
end
