import { Button, Header, Navbar } from "rsuite";
import ArowBack from "@rsuite/icons/ArowBack";
import { useNavigate } from "react-router-dom";
import Logo from "../logo.png";
import "./Page404.css";

export const Page404 = () => {
  let navigate = useNavigate();
  return (
    <>
      <Header>
        <Navbar
          appearance="inverse"
          style={{ background: "#0a2042", position: "fixed", width: "100%" }}
        >
          <Navbar.Brand style={{ textAlign: "left" }}>
            <span style={{ color: "#fff" }}>
              <img
                src={Logo}
                alt="logo"
                style={{
                  width: "100px",
                  marginTop: "-5px",
                  textAlign: "left",
                }}
              />
            </span>
          </Navbar.Brand>
        </Navbar>
      </Header>
      <div className="page404">
        <h1>404!</h1>
        <h2>This Page Doesn't Seem to Exist...</h2>
        <Button appearance="default" size="lg" onClick={() => navigate("/")}>
          <ArowBack style={{ marginRight: "10px" }} />
          Go Back
        </Button>
      </div>
    </>
  );
};
