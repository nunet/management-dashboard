/* eslint-disable testing-library/prefer-screen-queries */
import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { TableTabs } from '../../components/TableTabs';

describe('TableTabs Component', () => {
  it('renders all tabs', () => {
    const { getByText } = render(<TableTabs data={[]} active="withdraw" />);
    expect(getByText('Withdraw')).toBeInTheDocument();
  });
});
