import { capitalizeFirstLetter } from '../../helpers/Capitalize';

describe('capitalizeFirstLetter Function', () => {
  test('capitalizes the first letter of a lowercase string', () => {
    const lowercaseString = 'hello';

    const result = capitalizeFirstLetter(lowercaseString);

    expect(result).toEqual('Hello');
  });

  test('does not change the case of an already capitalized string', () => {
    const capitalizedString = 'World';

    const result = capitalizeFirstLetter(capitalizedString);

    expect(result).toEqual('World');
  });

  test('handles an empty string', () => {
    const emptyString = '';

    const result = capitalizeFirstLetter(emptyString);

    expect(result).toEqual('');
  });

  test('capitalizes the first letter of a mixed-case string', () => {
    const mixedCaseString = 'exAmple';

    const result = capitalizeFirstLetter(mixedCaseString);

    expect(result).toEqual('ExAmple');
  });
});
