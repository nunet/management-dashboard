# [0.2.0-alpha.1](https://gitlab.com/nunet/management-dashboard/compare/v0.1.8-alpha.1...v0.2.0-alpha.1) (2023-12-18)


### Bug Fixes

* unable to connect to DMS running in WSL using 127.0.0.1 ([e2ea62b](https://gitlab.com/nunet/management-dashboard/commit/e2ea62b810cc710d0778544480bd08b129294aec))


### Features

* added the transaction sync logic and removed reload ([3ec4e53](https://gitlab.com/nunet/management-dashboard/commit/3ec4e53a9bb6ce81c92d39912b3542d9fe2f1e68))

## [0.1.8-alpha.1](https://gitlab.com/nunet/management-dashboard/compare/v0.1.7...v0.1.8-alpha.1) (2023-10-24)


### Bug Fixes

* Configure nunet(non root) as user ([20eaca0](https://gitlab.com/nunet/management-dashboard/commit/20eaca094d917abd454d51ffeaf4782bb615e807))

## [0.2.2](#71)
### Added
- logic to use the new cardano node  

## [0.2.3](#21)
### Added
- unit tests for CPD

## [0.2.0](#14)
### Added
- transactions table 
- transactions operations
- filtering

## [0.1.7](#13)
### Added
- recaptcha error fixed
- unselect option added
- window sizing / margin fixed
- amounts to claim look weird (huge numbers) fixed

## [0.1.6](!43)
### Added

- Call to DMS /transactions to get list of hashes for jobs done
- Success/Fail message after withdrawal
### Changed

- TxHash included with request-reward and send-status
- Filter claimable tokens using txHashes from the DMS
- Appearance of claimable tokens table

## [0.1.5](https://gitlab.com/nunet/management-dashboard/-/tree/v0.1.5)

### Fixed
- Missing contract inputs
